import pymel.core as pm


pm.loadPlugin('mtoa.mll', quiet=True)

# Quiet load alembic plugins
pm.loadPlugin('AbcExport.mll', quiet=True)
pm.loadPlugin('AbcImport.mll', quiet=True)
